from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import time
import misc.utils as utils
from collections import OrderedDict
import torch
from torch.autograd import Variable
import sys
sys.path.append('coco_caption')
#from coco_caption.pyciderevalcap.ciderD.ciderD import CiderD

from cider.pyciderevalcap.ciderD.ciderD import CiderD
CiderD_scorer = CiderD(df='para-train-idxs')
#CiderD_scorer = CiderD(df='corpus')


def array_to_str(arr,add_eos=True):  # arr:[6,max_len]
    out = ''


    for i in range(len(arr)):
        sent = arr[i]
        if sent.sum() == 0:
            break
        for j in range(len(sent)):
            if sent[j] == 0:
                if add_eos:
                   out = out + '2 '
                break
            else:
                out += str(sent[j]) + ' '

    return out.strip()

def get_self_critical_reward(alpha,model,dis_model,dis_label,feats,topic,var_copy, copy_flag, data, gen_result):


    batch_size = gen_result.size(0)  # batch
    sent_num = topic.size(1)
    model.eval()
    # get greedy decoding baseline
    with torch.no_grad():

         greedy_res, _,_ = model.sample(feats,var_copy, copy_flag)#greedy_res：[batch,6,max(len(seq)]
    model.train()
    res = OrderedDict()

    gen_result_ = gen_result.cpu().numpy()
    greedy_res_ = greedy_res.cpu().numpy()
    for i in range(batch_size):
        res[i] = [array_to_str(gen_result_[i])]
    for i in range(batch_size):
        res[batch_size + i] = [array_to_str(greedy_res_[i])]

    gts = OrderedDict()
    for i in range(len(data['gts'])):

        gts[i] = [array_to_str(data['gts'][i],False)]

    # _, scores = Bleu(4).compute_score(gts, res)
    # scores = np.array(scores[3])
    res = [{'image_id':i, 'caption': res[i]} for i in range(2 * batch_size)]
    # batch_size = batch*seq_per_img
    gts = {i: gts[i % batch_size] for i in range(2 * batch_size)}
    _, scores = CiderD_scorer.compute_score(gts, res)
    sc_scores = scores[:batch_size] - scores[batch_size:]

    # dis_score: [batch_real*2,2]  sent_place:[batch]  Note: batch_real!=batch
    dis_model.eval()
    with torch.no_grad():
         _, dis_score, sent_index = dis_model(greedy_res, topic, gen_result, dis_label,mode='train_generator')
    batch_real = int(dis_score.size(0)*0.5)
    dis_score = dis_score.detach().cpu().numpy()  # [batch_real*2,2]


    adv_score_ = dis_score[:batch_real,1] - dis_score[batch_real:2 * batch_real,1]


    adv_reward = np.zeros([batch_size, sent_num], dtype='float32')
    current_ptr = 0
    for cnt,n in enumerate(sent_index.detach().cpu().numpy()):  # sent_index:[batch, sent_max]
        tmp_num = sum(n)
        ind = n.nonzero()
        adv_reward[cnt,[ind]] = adv_score_[current_ptr:(current_ptr+tmp_num)]
        current_ptr = current_ptr+tmp_num

    print('adv_score', adv_score_[:10])

    sc_reward = np.repeat(sc_scores[:, np.newaxis], gen_result.shape[1], 1)  # [batch,6]
    rewards = (alpha*adv_reward+(1-alpha)*sc_reward)*1.5

    rewards = np.repeat(rewards[:,:, np.newaxis], gen_result.shape[2], 2)  # [batch,6,29]
    return rewards, np.mean(adv_reward), np.mean(sc_reward)