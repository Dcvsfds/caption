import torch
import numpy as np
from torch.autograd import Variable
import torch.optim as optim
import time
import os
from six.moves import cPickle
import misc.utils as utils
import eval_utils
import opts
from tensorboardX import SummaryWriter
from dataloader_gen import DataLoader_Gen
from models.HRNN import HRNN
from models.discriminator import Discriminator,generate_batch_sample
from misc.rewards import get_self_critical_reward
from models.adver_training import adv_train
os.environ['CUDA_VISIBLE_DEVICES'] = '0'



def pretrain_generator(opt,model):
    logger = utils.set_log(mode='xe')
    loader = DataLoader_Gen(opt)
    # writer = SummaryWriter('log')
    opt.vocab_size = loader.vocab_size
    opt.ix_to_word = loader.get_vocab()
    infos = {}
    if opt.start_from is not None:
        with open(os.path.join(opt.start_from,'infos.pkl'),'rb') as f:
            infos = cPickle.load(f)
            save_model_opt = infos['opt']
            need_be_same = ['word_rnn_size', 'sen_rnn_size', 'topic_dim']
            for checkme in need_be_same:
                assert vars(save_model_opt)[checkme] == vars(opt)[
                    checkme], "Command line argument and saved model disagree on '%s' " % checkme
    iteration = infos.get('iter', 0)
    epoch = infos.get('epoch', 0)


    loader.iterators = infos.get('iterators', loader.iterators)
    loader.split_ix = infos.get('split_ix', loader.split_ix)

    if opt.load_best_score == 1:
        best_val_score = infos.get('best_val_score', None)

    if vars(opt).get('start_from', None) is not None:
        # check if all necessary files exist
        assert os.path.isdir(opt.start_from), " %s must be a a path" % opt.start_from
        assert os.path.isfile(os.path.join(opt.start_from,
                                           "infos.pkl")), "infos.pkl file does not exist in path %s" % opt.start_from
        model.load_state_dict(torch.load(os.path.join(opt.start_from, 'model.pth')))
    update_lr_flag = True
    model.train()
    crit = utils.Criterion(opt)
    rl_crit = utils.RewardCriterion()
    optimizer = optim.Adam(model.parameters(),lr=opt.learning_rate)
    if vars(opt).get('start_from',None) is not None and os.path.isfile(os.path.join(opt.start_from,'optimizer.pth')):
        print('load optimizer.pth')
        optimizer.load_state_dict(torch.load(os.path.join(opt.start_from,'optimizer.pth')))
    eval_kwargs = {'split': 'val', 'dataset': opt.input_json, 'verbose': True}
    eval_kwargs.update(vars(opt))
    epoch_start = time.time()
    loss_epoch = []
    while True:
        if update_lr_flag:
            if epoch >= opt.learning_rate_decay_start and opt.learning_rate_decay_start >=0:
                frac = (epoch-opt.learning_rate_decay_start) // opt.learning_rate_decay_every
                decay_factor = opt.learning_rate_decay_rate**frac
                opt.current_lr = opt.pretrain_lr_generator * decay_factor
                utils.set_lr(optimizer,opt.current_lr)
            else:
                opt.current_lr = opt.pretrain_lr_generator
            if epoch  > opt.scheduled_sampling_start and opt.scheduled_sampling_start >= 0:
                frac = (epoch-opt.scheduled_sampling_start) // opt.scheduled_sampling_increase_every
                opt.ss_prob = min(opt.scheduled_sampling_increase_prob*frac, opt.scheduled_sampling_max_prob)
                model.ss_prob = opt.ss_prob
                update_lr_flag = False

        start = time.time()
        torch.cuda.synchronize()
        data = loader.get_batch('train')
        if epoch >= opt.copynet_after:
            copy_flag = True
        else:
            copy_flag = False

        feats, labels, masks, sent_place, var_copy, dis_label = utils.input_data(data)

        optimizer.zero_grad()
        predict_word, predict_end = model(feats, labels, masks, var_copy, copy_flag)
        loss_word, loss_end, loss = crit(predict_word, predict_end, labels[:, :, 1:], sent_place, masks[:, :, 1:],
                                         loader)

        loss.backward()
        loss_epoch.append(loss.item())
        total_norm = 0

        optimizer.step()
        train_loss = loss.item()
        torch.cuda.synchronize()
        end = time.time()
        if iteration % 50 == 0:
            logger.info("iter {}  epoch {} , train_loss {:.3f},loss_word {:.3f},loss_end {:.3f}, total_norm = {:.3f}, time/batch = {:.3f}" \
                    .format(iteration, epoch, train_loss, loss_word.item(), loss_end.item(), total_norm, end - start))
            print(
                "iter {}  epoch {} , train_loss {:.3f},loss_word {:.3f},loss_end {:.3f}, total_norm = {:.3f}, time/batch = {:.3f}" \
                    .format(iteration, epoch, train_loss, loss_word.item(), loss_end.item(), total_norm, end - start))
            # writer.add_scalar('train/train_loss', train_loss, iteration)


        iteration = iteration+1
        if iteration % opt.save_checkpoint_every == 0:
           eval_kwargs = {'split':'val', 'dataset':opt.input_json, 'verbose':True}
           eval_kwargs.update(vars(opt))
           val_loss_word, val_loss_end, val_loss, predictions,lang_stats = eval_utils.eval_split(model,crit, loader,copy_flag, eval_kwargs)
           logger.info(
               "Bleu_1 is {0:.3f}, Bleu_2 is {1:.3f},Bleu_3 is {2:.3f}, Bleu_4 is {3:.3f},METEOR is {4:.3f}, ROUGE_L is {5:.3f}, CIDEr is {6:.3f} " \
               .format(lang_stats['Bleu_1'], lang_stats['Bleu_2'], lang_stats['Bleu_3'], lang_stats['Bleu_4'],
                       lang_stats['METEOR'], lang_stats['ROUGE_L'], lang_stats['CIDEr']))
           # writer.add_scalar('val_score/Bleu1', lang_stats['Bleu_1'], iteration)
           # writer.add_scalar('val_score/Bleu4', lang_stats['Bleu_4'], iteration)
           # writer.add_scalar('val_score/METEOR', lang_stats['METEOR'], iteration)
           # writer.add_scalar('val_score/CIDEr', lang_stats['CIDEr'], iteration)


           if opt.language_eval == 1:
               current_score = lang_stats['CIDEr']

           best_flag = False
           if True:
               if best_val_score is None or current_score > best_val_score:
                   best_val_score = current_score
                   best_flag = True

               opt.checkpoint_path = opt.checkpoint_path_xe
               if not os.path.exists(opt.checkpoint_path):
                   os.makedirs(opt.checkpoint_path)
               checkpoint_path = os.path.join(opt.checkpoint_path, 'model.pth')
               torch.save(model.state_dict(), checkpoint_path)
               print("model saved to {}".format(checkpoint_path))

               optimizer_path = os.path.join(opt.checkpoint_path, 'optimizer.pth')
               torch.save(optimizer.state_dict(), optimizer_path)

               infos['iter'] = iteration
               infos['epoch'] = epoch
               infos['iterators'] = loader.iterators
               infos['split_ix'] = loader.split_ix
               infos['best_val_score'] = best_val_score
               infos['opt'] = opt
               infos['vocab'] = loader.get_vocab()

               with open(os.path.join(opt.checkpoint_path, 'infos.pkl'), 'wb') as f:
                   cPickle.dump(infos, f)
               if best_flag:
                   checkpoint_path = os.path.join(opt.checkpoint_path, 'model-best.pth')
                   torch.save(model.state_dict(), checkpoint_path)
                   print("model saved to {}".format(checkpoint_path))

                   optimizer_path = os.path.join(opt.checkpoint_path, 'optimizer-best.pth')
                   torch.save(optimizer.state_dict(), optimizer_path)
                   with open(os.path.join(opt.checkpoint_path, 'infos-best.pkl'), 'wb') as f:
                       cPickle.dump(infos, f)

        if data['bounds']['wrapped']:
            loss_per = np.mean(np.array(loss_epoch))
            loss_epoch = []
            # writer.add_scalar('loss/loss_epoch', loss_per, epoch)
            epoch += 1
            update_lr_flag = True
            print("epoch: " + str(epoch) + " during: " + str(time.time() - epoch_start))
            epoch_start = time.time()

        if epoch >= opt.max_mle_epochs and opt.max_mle_epochs != -1:
            break


def pretrain_discriminator(opt,model,dis_model):  # train discriminator on compeleted dataset
    logger = utils.set_log(mode='dis')
    model.eval()
    dis_model.train()
    from dataloader_dis import DataLoader_Dis  # load pre-processed features
    loader_dis = DataLoader_Dis(opt)
    iteration = 0
    epoch = 0

    dis_optimizer = optim.Adam(dis_model.parameters(), lr=opt.pretrain_lr_dis)
    total_loss=0
    while True:
        start = time.time()
        torch.cuda.synchronize()
        data = loader_dis.get_sample_batch('train',random_sample=False)
        copy_flag = True
        dis_optimizer.zero_grad()
        seq, topic, gt, dis_label = generate_batch_sample(model,data,copy_flag,opt)
        dis_loss,_,_ = dis_model(seq, topic, gt, dis_label,mode='train_dis')
        dis_loss.backward()
        dis_optimizer.step()
        train_loss = dis_loss.item()

        torch.cuda.synchronize()
        end = time.time()
        total_loss = total_loss+train_loss
        iteration = iteration+1
        if iteration % opt.log_dis_loss_every ==0:

            total_loss = total_loss/opt.log_dis_loss_every
            logger.info("iter {} (epoch {}),dis_loss = {:.3f}" \
                         .format(iteration, epoch, total_loss))
            print("iter {} (epoch {}),dis_loss = {:.3f},time/batch = {:.3f}" \
                  .format(iteration, epoch, total_loss, end - start))
            # writer.add_scalar('Train/dis_loss', total_loss, iteration)
            total_loss = 0
        if data['bounds']['wrapped']:
            epoch += 1
            # save dis_model every epoch
            dis_checkpoint_path = os.path.join(opt.checkpoint_path_xe, 'dis-model-pretrain.pth')
            torch.save(dis_model.state_dict(), dis_checkpoint_path)
            print("dis model saved to {}".format(dis_checkpoint_path))
        if epoch>=opt.pretrain_dis_epoch:
            break

def main():
    opt = opts.parse_opt()
    opt.batch_size=60

    opt.learning_rate_decay_start = 0
    opt.scheduled_sampling_start = 1

    opt.save_checkpoint_every = 200
    opt.val_images_use=5000


    opt.language_eval = 1
    opt.input_json = 'data/paratalk.json'
    opt.input_label_h5 = 'data/paratalk_label.h5'
    opt.input_feature = 'VG_feature.h5'

    # model setting, generator
    model = HRNN(opt)
    model.cuda()
    # discriminator
    dis_model = Discriminator(opt)
    dis_model.cuda()

    # mle_setting
    opt.pretrain_lr_generator = 5e-4
    opt.copynet_after = 5
    opt.max_mle_epochs = 60  # 30
    opt.checkpoint_path_xe = 'save_xe'

    # discriminator setting
    opt.pretrain_lr_dis = 1e-4
    opt.log_dis_loss_every = 100  # 200
    opt.pretrain_dis_epoch = 10

    # adversarial training
    opt.scst_lr_generator = 5e-5
    opt.scst_lr_dis = 1e-5
    opt.checkpoint_path_scst = 'save_scst'
    opt.copy_adv = True
    opt.adv_trian_epoch = 50
    opt.score_alpha = 0.2

    opt.start_from = 'save_scst'
    print(' start pretrain generator ')
    pretrain_generator(opt,model)
    # start pretrain discriminator
    print('init discriminator embed with generator')
    pretrain_discriminator(opt, model,dis_model)
    # start Adversarial Training
    adv_train(opt, model, dis_model)

main()
