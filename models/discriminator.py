from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import *
import misc.utils as utils
from collections import OrderedDict
import numpy as np
import random
import json


class caption_lstm(nn.Module):   # used for GT caption or generated caption by generator
    def __init__(self,opt):
        super(caption_lstm,self).__init__()
        self.info = json.load(open(opt.input_json))
        self.paragraph_i2w = self.info['ix_to_word']
        self.vocab_size = len(self.paragraph_i2w)

        self.input_encoding_size = opt.word_encoding_size  # 512
        self.drop_prob_lm = opt.drop_prob_lm
        self.rnn_size = opt.caption_rnn_size
        self.caption_embed = nn.Sequential(nn.Embedding(self.vocab_size, self.input_encoding_size),
                                   nn.Dropout(self.drop_prob_lm))


        self.dis_lstm = nn.LSTMCell(opt.caption_rnn_size, opt.caption_rnn_size)

        self.rnn = nn.LSTM(
            input_size=self.input_encoding_size,
            hidden_size=self.rnn_size,
            num_layers=1,
            bidirectional=False,
            batch_first=True,
            dropout=self.drop_prob_lm)
    def init_state(self, batch_size):
        h0_encoder = Variable(torch.zeros(
            batch_size,
            self.rnn_size
        ), requires_grad=False)

        c0_encoder = Variable(torch.zeros(
            batch_size,
            self.rnn_size
        ), requires_grad=False)
        if torch.cuda.is_available():
            return h0_encoder.cuda(), c0_encoder.cuda()
        return h0_encoder, c0_encoder

    def _argsort(self, seq):
        return sorted(range(len(seq)), key=seq.__getitem__)
    def _process_lengths(self, input):
        max_length = input.size(1)
        lengths = list(max_length - input.data.eq(0).sum(1).squeeze())
        return lengths
    def forward(self,img, seq,lengths=None):  # seq:[batch*5*4,max_length]
        batch_size = seq.size(0)  # batch*5*4
        h0, c0 = self.init_state(batch_size)
        h_img, c_img = self.dis_lstm(img, (h0, c0))

        if lengths is None:
            lengths = self._process_lengths(seq)
        sorted_lengths = sorted(lengths)
        sorted_lengths = sorted_lengths[::-1]
        idx = self._argsort(lengths)
        idx = idx[::-1]  # decreasing order
        inverse_idx = self._argsort(idx)
        idx = Variable(torch.LongTensor(idx))
        inverse_idx = Variable(torch.LongTensor(inverse_idx))
        if seq.data.is_cuda:
            idx = idx.cuda()
            inverse_idx = inverse_idx.cuda()
        x = torch.index_select(seq, 0, idx)
        h0_idx = torch.index_select(h_img, 0, idx).unsqueeze(0)
        c0_idx = torch.index_select(c_img, 0, idx).unsqueeze(0)

        x = self.caption_embed(x)
        x = nn.utils.rnn.pack_padded_sequence(x, sorted_lengths, batch_first=True)
        x, (src_h_t, src_c_t) = self.rnn(x, (h0_idx, c0_idx))

        h_t = src_h_t[-1]
        c_t = src_c_t[-1]
        hn = torch.index_select(h_t, 0, inverse_idx)  # [batch*evidence_num,evidence_rnn_size]+
        # just use the last-step hidden state
        return hn


def generate_batch_sample(model, data, copy_flag, opt):
    model.eval()
    feats, labels, masks, sent_place, var_copy, dis_label = utils.input_data(data)
    seq, _, topic = model.sample(feats,var_copy,copy_flag, opt={'sample_max': 0})

    lab = labels[:, :, 1:]
    label = torch.where(lab != 2, lab, torch.full_like(lab, 0))
    return seq, topic, label, dis_label

class highway(nn.Module):
    def __init__(self,opt):
        super(highway,self).__init__()
        self.opt = opt
        self.highway_h = nn.Sequential(nn.Linear(opt.caption_rnn_size, opt.caption_rnn_size),
                                    nn.ReLU())
        self.highway_t = nn.Sequential(nn.Linear(opt.caption_rnn_size, opt.caption_rnn_size),
                                    nn.Sigmoid())
        self.drop = nn.Dropout(0.5)
    def forward(self, input):
        # highway network y= H(Wx+b)*T(Wx+b) + x*(1-T(Wx+b))  # H:relu  T:sigmoid
        y = self.highway_h(input) * self.highway_t(input) + (1 - self.highway_t(input)) * input  # [batch*5+fake_nums,caption_rnn_size]
        return self.drop(y)


class Discriminator(nn.Module):
    def __init__(self,opt):
        super(Discriminator,self).__init__()
        self.opt = opt
        self.caption_rnn_size = opt.caption_rnn_size
        self.cap_lstm = caption_lstm(opt)
        self.img_sapce = nn.Sequential(nn.Linear(opt.word_rnn_size, opt.caption_rnn_size),
                                       nn.ReLU(),
                                       nn.Dropout(0.3))
        self.cat2hidden = nn.Sequential(nn.Linear(opt.caption_rnn_size*6, opt.caption_rnn_size),
                                       nn.LeakyReLU(0.3),
                                       nn.Dropout(0.3))
        self.dis_fuse = nn.Sequential(nn.Linear(opt.caption_rnn_size*5, opt.caption_rnn_size),
                                       nn.LeakyReLU(0.3),
                                       nn.Dropout(0.3))
        self.cat_caption = nn.Sequential(nn.Linear(opt.caption_rnn_size*2, opt.caption_rnn_size),
                                       nn.LeakyReLU(0.3),
                                       nn.Dropout(0.3))
        self.highway = highway(opt)
        self.hidden2out = nn.Sequential(nn.Linear(opt.caption_rnn_size , 2),nn.Sigmoid())
        self.loss  = nn.CrossEntropyLoss()
        # self.loss = nn.BCEWithLogitsLoss()
        self.softmax = nn.Softmax(dim=1)
        self.sigmoid = nn.Sigmoid()
    def _argsort(self, seq):
        return sorted(range(len(seq)), key=seq.__getitem__)

    def init_embed(self,model):
        model.eval()
        embed_weights = list(model.state_dict().values())[14].data.cpu().numpy()
        self.cap_embed_weights = torch.Tensor(embed_weights)
        self.cap_dict =OrderedDict({'0.weight':self.cap_embed_weights})

        self.cap_lstm.caption_embed.load_state_dict(self.cap_dict)

    def flip(self,x):  # flip a tensor
        idx = [i for i in range(x.size(0) - 1, -1, -1)]
        idx = torch.LongTensor(idx)
        if x.data.is_cuda:
            idx = idx.cuda()
        inverted_tensor = x.index_select(0, idx)
        return inverted_tensor

    def process_data(self,generated_seq, topic_vector, gt_caption, dis_label):

        batch = generated_seq.size(0)
        sent_max = generated_seq.size(1)
        dis_label = dis_label.contiguous().view(batch,-1,dis_label.size(1))

        seq_place = generated_seq.sum(dim=2)
        gt_place = gt_caption.sum(dim=2)

        last_index = generated_seq.data.new(batch, sent_max).zero_()  # [batch,6]

        for i in range(batch):
            for j in range(sent_max):
                if seq_place[i][j] == 0 or gt_place[i][j] == 0:
                    continue
                else:
                    last_index[i,j]=1

        batch_real = last_index.sum()
        gene_tmp = Variable(generated_seq.data.new(batch_real, generated_seq.size(2)).zero_())
        topic_tmp = Variable(topic_vector.data.new(batch_real, topic_vector.size(2)).zero_())
        gt_tmp = Variable(gt_caption.data.new(batch_real, gt_caption.size(2)).zero_())
        dis_tmp = Variable(dis_label.data.new(batch_real, self.opt.dis_num, dis_label.size(2)).zero_())
        cnt = 0
        for i in range(batch):
            ptr = last_index[i].sum().data
            nonzero_idx = (last_index[i]>0).nonzero().view(-1)
            gene_tmp[cnt:(ptr + cnt)] = generated_seq[i].index_select(0,nonzero_idx)
            topic_tmp[cnt:(ptr + cnt)] = topic_vector[i].index_select(0,nonzero_idx)
            gt_tmp[cnt:(ptr + cnt)] = gt_caption[i].index_select(0,nonzero_idx)
            dis_cur = dis_label[i].unsqueeze(0).expand(ptr, dis_label.size(1), dis_label.size(2))  # [ptr,5, word_max-1]
            dis_tmp[cnt:(ptr + cnt)] = dis_cur
            cnt = cnt + ptr

        dis_tmp = dis_tmp.contiguous().view(-1, dis_tmp.size(2))  # # [batch_real*5, word_max-1]
        return gene_tmp, topic_tmp, gt_tmp, dis_tmp, last_index


    def forward(self, generated_seq, topic_vector, gt_caption, dis_label,mode):


        batch = generated_seq.size(0)


        gene_tmp, topic_tmp, gt_tmp, dis_tmp, last_index = self.process_data(generated_seq, topic_vector, gt_caption, dis_label)

        batch_real = gene_tmp.size(0)
        # print(batch_real)
        fake_nums = int(batch_real*0.2)
        target = torch.ones(batch_real * 2+fake_nums, dtype=torch.long).cuda()

        target[batch_real:] = 0
        # make shuffle index
        idx = np.arange(target.size()[0])
        np.random.shuffle(idx)
        inverse_idx = np.argsort(idx)

        image_hidden = self.img_sapce(topic_tmp)
        img_fake = self.flip(image_hidden)  # reverse img to make fake imgs
        img_fake = img_fake[:fake_nums, :]  # [fake_nums,cap_rnn_size]
        # concat for gt, generated_seq, and fake
        img_inner = torch.cat((image_hidden,image_hidden,img_fake),0)

        seq_inner = torch.cat((gt_tmp, gene_tmp, gt_tmp[-fake_nums:]), 0)  # [batch_real*2+fake_num,word_max-1]
        hidden_inner = self.cap_lstm(img_inner, seq_inner)

        # cancat for dis_label
        img_outer = image_hidden.unsqueeze(1).expand(
                        image_hidden.size(0), self.opt.dis_num,image_hidden.size(1)).contiguous().view(
                        image_hidden.size(0)*self.opt.dis_num, image_hidden.size(1))  # [batch_real*5,cap_rnn_size]

        seq_outer = dis_tmp
        hidden_outer = self.cap_lstm(img_outer, seq_outer)
        hidden_outer = hidden_outer.contiguous().view(
                                    batch_real,-1)
        gt_hidden = hidden_inner[:batch_real]
        gene_hidden = hidden_inner[batch_real:batch_real*2]
        fake_hidden = hidden_inner[batch_real*2:]
        pos = torch.cat((gt_hidden,hidden_outer),1)
        neg = torch.cat((gene_hidden,hidden_outer),1)
        fake = torch.cat((fake_hidden,hidden_outer[-fake_nums:]),1)

        input = torch.cat((pos,neg,fake),0)
        highway_input = self.cat2hidden(input)
        y = self.highway(highway_input)
        out = self.hidden2out(y)
        score = self.softmax(out)


        loss = self.loss(out, target)

        return loss, score[:batch_real * 2], last_index