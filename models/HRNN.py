import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import *
import math
import numpy as np
import json
from collections import OrderedDict
from .CaptionModel import CaptionModel

class Word_LSTM(nn.Module):

    def __init__(self, opt, use_maxout=False):
        super(Word_LSTM, self).__init__()
        self.lang_drop = opt.lang_drop
        #                                             512 + 512 × 2                  512
        self.att_lstm = nn.LSTMCell(opt.word_encoding_size + opt.word_rnn_size * 2, opt.word_rnn_size)  # we, fc, h^2_t-1
        self.lang_lstm = nn.LSTMCell(opt.word_rnn_size * 2, opt.word_rnn_size)  # h^1_t, \hat v

    def forward(self, xt, fc_feats,guide_vector, visual_value, visual_key, state,kv_attention,visual_kv_state, round_d):

        prev_h = state[0][-1]  # [batch,512]
        att_lstm_input = torch.cat([prev_h, fc_feats, xt], 1)
        h_att, c_att = self.att_lstm(att_lstm_input, (state[0][0], state[1][0]))
        visual_context, visual_weight, visual_kv_state = kv_attention(h_att, xt,guide_vector, visual_key, visual_value, visual_kv_state, round_d)

        lang_lstm_input = torch.cat([visual_context, h_att], 1)  # [batch,512×2]
        # h_lang：[batch,512]    c_lang：[batch,512]
        h_lang, c_lang = self.lang_lstm(lang_lstm_input, (state[0][1], state[1][1]))
        output = F.dropout(h_lang, self.lang_drop, self.training)  # [batch*seq_per_img,512]
        state = (
        torch.stack([h_att, h_lang]), torch.stack([c_att, c_lang]))  # ([2,batch*seq_per_img,512],[2,batch*seq_per_img,512])

        return output, state,visual_context, visual_kv_state




class Attention(nn.Module):
    def __init__(self, opt):
        super(Attention, self).__init__()
        self.rnn_size = opt.word_rnn_size
        self.att_hid_size = opt.att_hid_size

        self.h2att = nn.Linear(self.rnn_size, self.att_hid_size)
        self.alpha_net = nn.Linear(self.att_hid_size, 1)

    def forward(self, h, att_feats, p_att_feats, att_masks=None):
        # The p_att_feats here is already projected
        att_size = att_feats.numel() // att_feats.size(0) // att_feats.size(-1)
        att = p_att_feats.view(-1, att_size, self.att_hid_size)

        att_h = self.h2att(h)
        att_h = att_h.unsqueeze(1).expand_as(att)
        dot = att + att_h
        dot = F.tanh(dot)
        dot = dot.view(-1, self.att_hid_size)
        dot = self.alpha_net(dot)
        dot = dot.view(-1, att_size)

        weight = F.softmax(dot, dim=1)
        if att_masks is not None:
            weight = weight * att_masks.view(-1, att_size).float()
            weight = weight / weight.sum(1, keepdim=True)
        att_feats_ = att_feats.view(-1, att_size, att_feats.size(-1))
        att_res = torch.bmm(weight.unsqueeze(1), att_feats_).squeeze(1)

        return att_res,weight

class Guide_Attention(nn.Module):
    def __init__(self, opt):
        super(Guide_Attention, self).__init__()
        self.rnn_size = opt.word_rnn_size
        self.att_hid_size = opt.att_hid_size

        self.h2att = nn.Linear(self.rnn_size, self.att_hid_size)
        self.alpha_net = nn.Linear(self.att_hid_size, 1)

    def forward(self, h, att_feats, p_att_feats, att_masks=None):
        att_size = att_feats.numel() // att_feats.size(0) // att_feats.size(-1)
        att = p_att_feats.view(-1, att_size, self.att_hid_size)

        att_h = self.h2att(h)
        att_h = att_h.unsqueeze(1).expand_as(att)
        dot = att + att_h
        dot = F.tanh(dot)
        dot = dot.view(-1, self.att_hid_size)
        dot = self.alpha_net(dot)
        dot = dot.view(-1, att_size)

        weight = F.softmax(dot, dim=1)
        if att_masks is not None:
            weight = weight * att_masks.view(-1, att_size).float()
            weight = weight / weight.sum(1, keepdim=True)
        att_feats_ = att_feats.view(-1, att_size, att_feats.size(-1))
        att_res = torch.bmm(weight.unsqueeze(1), att_feats_).squeeze(1)

        return att_res,weight




class Copy_Attention(nn.Module):
    def __init__(self, enc_dim, trg_dim):
        super(Copy_Attention, self).__init__()

        self.attn = nn.Linear(enc_dim, trg_dim)
        self.softmax = nn.Softmax()
        self.linear_out = nn.Linear(enc_dim + trg_dim, trg_dim, bias=False)
        self.tanh = nn.Tanh()
        self.relu = nn.ReLU()
    def forward(self, hidden, encoder_outputs, encoder_mask):
        '''
        :param hidden: (batch_size*5, 1, trg_dim)  一个step计算一次copy_attention
        :param encoder_outputs: (batch_size*5, similar_len, enc_dim)
        :return:
            attn_energies  (batch_size*5, 1, similar_len): the attention energies before softmax
        '''

        energies = self.attn(encoder_outputs)
        attn_energies = torch.bmm(hidden, energies.transpose(1, 2))
        attn_energies = attn_energies * encoder_mask.view(encoder_mask.size(0), 1, encoder_mask.size(1))
        return self.relu(attn_energies)



class HRNN(CaptionModel):
    def __init__(self, opt):
        super(HRNN,self).__init__()
        self.opt = opt
        self.info = json.load(open(self.opt.input_json))
        self.paragraph_i2w = self.info['ix_to_word']
        self.vocab_size = len(self.paragraph_i2w)

        self.feat_drop = opt.feat_drop
        self.lang_feat = opt.lang_drop
        self.word_rnn_size = opt.word_rnn_size
        self.sent_max = opt.sen_max
        self.word_max = opt.word_max
        self.round = opt.round_size
        self.ss_prob = 0.0
        self.word_encoding_size = opt.word_encoding_size  # 1024
        self.feats_dim = opt.feats_dim  # 4096
        self.sen_rnn_size = opt.sen_rnn_size  # 512
        self.feats_pool_dim = opt.feats_pool_dim  # 1024
        self.att_hid_size = opt.att_hid_size  # 512
        self.sen_lstm = nn.LSTMCell(self.feats_pool_dim, self.sen_rnn_size)
        self.feats_pool = nn.Linear(self.feats_dim,self.feats_pool_dim)
        self.feat_topic_embed = nn.Sequential(self.feats_pool, nn.ReLU(),)
        self.pre_prob = nn.Linear(self.sen_rnn_size, 2)  # 0 is stop and 1 is continue

        # Word RNN
        self.topic_size = opt.topic_dim
        self.topic_layer = nn.Sequential(nn.Linear(self.sen_rnn_size, self.sen_rnn_size),nn.ReLU(),
                                         nn.Linear(self.sen_rnn_size, self.word_rnn_size),nn.ReLU(),
                                         nn.Dropout(self.feat_drop)
                                        )
        self.Embedding = nn.Embedding(self.vocab_size, self.word_rnn_size)
        self.embed = nn.Sequential(self.Embedding,#nn.ReLU())
                                   nn.Dropout(self.lang_feat))
        self.word_lstm = Word_LSTM(opt)
        self.logit = nn.Linear(self.word_rnn_size, self.vocab_size-2)  # delete <PAD> and <EOS>

        self.softmax = nn.Softmax(dim=1)
        #self.init_from_dense()


        self.kv_lstm = nn.LSTMCell(self.word_encoding_size+self.word_rnn_size*2, self.word_rnn_size)
        self.visual_att = Attention(opt)
        self.att_embed = nn.Sequential(
            nn.Linear(self.feats_dim, self.word_rnn_size),
            nn.ReLU(), nn.Dropout(self.feat_drop))
        self.ctx2att = nn.Linear(self.word_rnn_size, self.att_hid_size)
        self.add_gate_topic = nn.Linear(self.word_rnn_size, self.word_rnn_size)
        self.forget_gate_topic = nn.Linear(self.word_rnn_size, self.word_rnn_size)
        self.add_gate_att = nn.Linear(self.word_rnn_size, self.word_rnn_size)
        self.forget_gate_att = nn.Linear(self.word_rnn_size, self.word_rnn_size)
        self.sigmoid = nn.Sigmoid()

        self.num_directions = 2 if opt.bidirectional else 1
        # copynet
        self.copy_encoder = nn.LSTM(
            input_size=self.word_encoding_size,
            hidden_size=self.word_rnn_size,
            num_layers=self.opt.nlayers_copy,
            bidirectional=self.opt.bidirectional,
            batch_first=True,
            dropout=0.5)
        self.copy_att = Copy_Attention(self.word_rnn_size * self.num_directions, self.word_rnn_size)
        self.guide_att = Guide_Attention(opt)
        self.copy_guide_1 = nn.Linear(self.word_rnn_size * self.num_directions, self.word_rnn_size)
        self.copy_guide_2 = nn.Linear(self.word_rnn_size * self.num_directions, self.word_rnn_size)
        self.global_mean = nn.Linear(self.opt.feats_dim, self.word_rnn_size)
        self.generating_factor = nn.Linear(self.word_rnn_size*2, 1)
    def init_sent_hidden(self, bsz):
        weight = next(self.parameters()).data
        return (Variable(weight.new(bsz, self.sen_rnn_size).zero_()),
                Variable(weight.new(bsz, self.sen_rnn_size).zero_()))
    def init_word_lstm2_hidden(self, bsz):
        weight = next(self.parameters()).data
        return (Variable(weight.new(bsz, self.sen_rnn_size).zero_()),
                Variable(weight.new(bsz, self.sen_rnn_size).zero_()))

    def init_copy_encoder_state(self, input):
            """Get cell states and hidden states."""
            batch_size = input.size(0) \
                if self.copy_encoder.batch_first else input.size(1)

            h0_encoder = Variable(torch.zeros(
                self.copy_encoder.num_layers * self.num_directions,
                batch_size,
                self.word_rnn_size
            ), requires_grad=False)

            c0_encoder = Variable(torch.zeros(
                self.copy_encoder.num_layers * self.num_directions,
                batch_size,
                self.word_rnn_size
            ), requires_grad=False)

            if torch.cuda.is_available():
                return h0_encoder.cuda(), c0_encoder.cuda()

            return h0_encoder, c0_encoder
    def copynet_encode(self, input_src, input_len_sort, input_src_len):
        """
        Propogate input through the network.
        """

        self.h0_encoder, self.c0_encoder = self.init_copy_encoder_state(input_src)  # (self.encoder.num_layers * self.num_directions, batch_size, self.src_hidden_dim)
        input_src = input_src[input_len_sort]
        input_src_len = input_src_len[input_len_sort]
        # input (batch_size, src_len), src_emb (batch_size, src_len, emb_dim)
        src_emb = self.embed(input_src)
        src_emb = nn.utils.rnn.pack_padded_sequence(src_emb, input_src_len, batch_first=True)

        # src_h (batch_size, seq_len, hidden_size * num_directions): outputs (h_t) of all the time steps
        # src_h_t, src_c_t (num_layers * num_directions, batch, hidden_size): hidden and cell state at last time step
        src_h, (src_h_t, src_c_t) = self.copy_encoder(
            src_emb, (self.h0_encoder, self.c0_encoder)
        )

        src_h, _ = nn.utils.rnn.pad_packed_sequence(src_h, batch_first=True)

        # concatenate to (batch_size, hidden_size * num_directions)
        if self.opt.bidirectional:
            h_t = torch.cat((src_h_t[-1], src_h_t[-2]), 1)
            c_t = torch.cat((src_c_t[-1], src_c_t[-2]), 1)
        else:
            h_t = src_h_t[-1]
            c_t = src_c_t[-1]
        # return src_h, (h_t, c_t)
        out = torch.zeros_like(src_h)
        for i in range(input_len_sort.size()[0]):
            out[input_len_sort[i], :, :] = src_h[i, :, :]
        return out

    def kv_attention(self,pre_hidden,pre_word,guide_vector,key,value,state, round_d):
        if round_d==0:
            kv_input = torch.cat((pre_hidden,pre_word,guide_vector),dim=1)
            query,c = self.kv_lstm(kv_input,state)
            state = torch.stack([query, c])
        else:
            query = state[0]
        att_res, att_weight = self.visual_att(query,value,key)
        return att_res,att_weight,state
    def updata_kv_step(self,h,key,value):
        _,weight = self.visual_att(h,value,key)
        add_weight = self.sigmoid(self.add_gate_topic(h))
        forget_weight = self.sigmoid(self.forget_gate_topic(h))
        add = torch.bmm(weight.unsqueeze(2), add_weight.unsqueeze(1))
        forget = torch.bmm(weight.unsqueeze(2), forget_weight.unsqueeze(1))
        key = key*(1-forget)+add
        return key, weight

    def get_logprobs_state(self, it, tmp_copy, topic_vector,guide_vector, visual_value, visual_key, kv_state, state):


        copy_flag, tmp_copy_label_tmp, tmp_copy_label, tmp_dis_label, tmp_copy_mask,\
                                           tmp_copy_freq, tmp_copy_hidden, dis_model = tmp_copy
        beam = tmp_copy_freq.size(0)

        xt = self.embed(it+2)
        for round_d in range(self.round):
            output, state, visual_context, kv_state = self.word_lstm(xt, topic_vector,guide_vector, visual_value, visual_key, state, self.kv_attention, kv_state,round_d)
            visual_key,_ = self.updata_kv_step(output, visual_key, visual_value)

        h_lang = state[0][-1]  # [batch,512] get the language LSTM hidden state
        h_lang = h_lang.unsqueeze(1).expand(
            *((h_lang.size(0), self.opt.copy_num) + h_lang.size()[1:])).contiguous().view(
            *((h_lang.size(0) * self.opt.copy_num,) + h_lang.size()[1:])).unsqueeze(1)  # # [beam*5,1,512]

        gen_factor = self.generating_factor(torch.cat([visual_context, output], 1))
        gen_factor = self.sigmoid(gen_factor)

        copy_prob = self.copy_att(h_lang, tmp_copy_hidden, tmp_copy_mask)  # (batch*5, 1, max(copy_len))
        copy_prob = copy_prob.view(beam, -1)  # (batch, max(copy_len)*5)

        gen_prob = self.softmax(self.logit(output))
        if copy_flag:
            gen_prob = gen_prob * gen_factor
            copy_prob = self.softmax(copy_prob * (1 - gen_factor))
            merge_prob = gen_prob.scatter_add_(1, tmp_copy_label, copy_prob)
        else:
            merge_prob = gen_prob

        logprobs = torch.log(merge_prob, dim=1)

        args = [topic_vector,guide_vector, visual_value, visual_key,kv_state]
        return logprobs, state, args

    def forward(self, feats, seq, seq_mask, var_copy, copy_flag=False):
        batch_size = feats.size(0)
        region_num =feats.size(1)
        feat_dim = feats.size(2)

        copy_hidden = self.copynet_encode(var_copy[0], var_copy[2],
                                          var_copy[3])

        copy_label = var_copy[0][:, :copy_hidden.size(1)]
        copy_mask = var_copy[1][:, :copy_hidden.size(1)]
        copy_freq = var_copy[4][:, :copy_hidden.size(1)]
        copy_freq = copy_freq.contiguous().view(batch_size,-1)
        copy_label = torch.where((copy_label-2)>0,copy_label-2,torch.
                                 full_like(copy_label, 0)).view(batch_size,-1)

        #**************add copying guide **************#
        copy_mean_hidden = copy_hidden.sum(dim=1)
        copy_mean_hidden = copy_mean_hidden/(copy_mask.sum(1).unsqueeze(1))
        copy_mean_hidden = copy_mean_hidden.contiguous().view(batch_size, -1,
                                    self.num_directions* self.word_rnn_size)
        copy_guide = self.copy_guide_1(copy_mean_hidden)
        p_copy_guide = self.copy_guide_2(copy_mean_hidden)



        sen_h, sen_c = self.init_sent_hidden(batch_size)
        pool_feats = self.feat_topic_embed(feats).max(dim=1)[0]
        predict_end = Variable(feats.data.new(batch_size, self.sent_max,2).zero_())
        predict_word = Variable(feats.data.new(batch_size, self.sent_max, seq.size(2) - 1,
                                          self.vocab_size-2).zero_())
        store_h = Variable(feats.data.new(batch_size, self.sent_max, self.sen_rnn_size).zero_())
        att_weights = Variable(feats.data.new(batch_size,
                                              seq.size(2) - 1,region_num).zero_())

        feats = feats.view(-1, feat_dim)
        mean_feats = self.global_mean(feats).view(batch_size, -1, self.word_rnn_size)
        mean_feats = mean_feats.mean(dim=1)

        att_feats = self.att_embed(feats).view(batch_size, -1, self.word_rnn_size)
        p_att_feats = self.ctx2att(att_feats)

        visual_key = p_att_feats
        visual_value = att_feats
        visual_kv_state = self.init_word_lstm2_hidden(batch_size)
        guide_vector, _ = self.guide_att(mean_feats, copy_guide, p_copy_guide)
        for i in range(self.sent_max):
            sen_h, sen_c = self.sen_lstm(pool_feats,(sen_h, sen_c))
            topic_vector = self.topic_layer(sen_h)  # [batch, topic_size=512]


            logic_end = self.pre_prob(sen_h)
            predict_end[:, i,:] = logic_end
            store_h[:, i, :] = sen_h

            init_word_lstm2_hidden = self.init_word_lstm2_hidden(batch_size)
            state = (init_word_lstm2_hidden, init_word_lstm2_hidden)
            for j in range(seq.size(2)-1):
                if self.training and j >= 1 and self.ss_prob > 0.0:
                        sample_prob = feats.data.new(batch_size).uniform_(0, 1)
                        samp_mask = sample_prob < self.ss_prob
                        if samp_mask.sum() == 0:
                            it = seq[:, i, j ].clone()
                        else:
                            sample_ind = samp_mask.nonzero().view(-1)
                            it = seq[:, i, j].data.clone()
                            prob_prev = torch.exp(predict_word[:, i, j-1].detach())
                            it.index_copy_(0, sample_ind,
                                           torch.multinomial(prob_prev, 1).view(-1).index_select(0, sample_ind)+2)
                else:
                    it = seq[:, i, j].clone()
                if j >= 1 and seq[:, i, j].data.sum() == 0:
                    break
                xt = self.embed(it)  # embed is for all words
                for round_d in range(self.round):
                    output, state,visual_context,visual_kv_state = self.word_lstm(xt, topic_vector,guide_vector, visual_value, visual_key, state,
                                                                   self.kv_attention, visual_kv_state,round_d)
                    visual_key, _ = self.updata_kv_step(output, visual_key, visual_value)

                h_lang = state[0][-1]  # [batch,512] get the language LSTM hidden state
                h_lang = h_lang.unsqueeze(1).expand(
                    *((h_lang.size(0), self.opt.copy_num) + h_lang.size()[1:])).contiguous().view(
                    *((h_lang.size(0) * self.opt.copy_num,) + h_lang.size()[1:])).unsqueeze(1)  # # [batch*5,1,512]

                gen_factor = self.generating_factor(torch.cat([visual_context, output], 1))
                gen_factor = self.sigmoid(gen_factor)

                copy_prob = self.copy_att(h_lang, copy_hidden, copy_mask)  # (batch*5, 1, max(copy_len))
                copy_prob = copy_prob.view(batch_size,-1)  # (batch, max(copy_len)*5)

                gen_prob = self.softmax(self.logit(output))
                if copy_flag:
                    gen_prob = gen_prob*gen_factor
                    copy_prob = self.softmax(copy_prob*(1-gen_factor))
                    merge_prob = gen_prob.scatter_add_(1, copy_label, copy_prob)
                else:
                    merge_prob = gen_prob

                output = torch.log(merge_prob, dim=1)
                predict_word[:, i, j] = output

        return predict_word, predict_end
    def sample_beam(self,feats,var_copy, copy_flag,dis_model=None,dis_label=None, opt={}):
        beam_size = opt.get('beam_size', 10)
        stop_value = opt.get('stop_value', 0.5)
        batch_size = feats.size(0)
        region_num = feats.size(1)

        # *****************add copy***************#
        copy_hidden = self.copynet_encode(var_copy[0], var_copy[2],
                                          var_copy[3])  # [batch*5,15,1024]
        # truncate copy_labels and copy_masks and to the same size
        copy_label = var_copy[0][:, :copy_hidden.size(1)]  # [batch*5,max(copy_len)]
        copy_mask = var_copy[1][:, :copy_hidden.size(1)]  # [batch*5,max(copy_len)]
        copy_freq = var_copy[4][:, :copy_hidden.size(1)]  # [batch*5,max(copy_len)]
        copy_freq = copy_freq.contiguous().view(batch_size, -1)  # (batch,  max(copy_len)*5)

        copy_label_tmp = torch.where((copy_label - 2) > 0, copy_label - 2, torch.
                                     full_like(copy_label, 0))  # [batch*5,max(copy_len)]
        copy_label = copy_label_tmp.view(batch_size, -1)
        # ***************************************#

        copy_mean_hidden = copy_hidden.sum(dim=1)
        copy_mean_hidden = copy_mean_hidden / (copy_mask.sum(1).unsqueeze(1))
        copy_mean_hidden = copy_mean_hidden.contiguous().view(batch_size, -1,
                                                              self.num_directions * self.word_rnn_size)
        copy_guide = self.copy_guide_1(copy_mean_hidden)
        p_copy_guide = self.copy_guide_2(copy_mean_hidden)


        feat_dim = feats.size(2)  # 4096
        pool_feats = self.feat_topic_embed(feats).max(dim=1)[0]  # [batch,1024]

        mean_feats = self.global_mean(feats).view(batch_size, -1, self.word_rnn_size)
        mean_feats = mean_feats.mean(dim=1)
        guide_vector, _ = self.guide_att(mean_feats, copy_guide, p_copy_guide)


        feats = feats.view(-1, feat_dim)
        att_feats = self.att_embed(feats).view(batch_size, -1, self.word_rnn_size)
        p_att_feats = self.ctx2att(att_feats)


        self.seq_length = self.word_max - 1
        seq = torch.LongTensor(batch_size,self.sent_max,self.seq_length, ).zero_()

        seqLogprobs = torch.FloatTensor(batch_size,self.sent_max, self.seq_length, )
        sent_end = torch.LongTensor(batch_size, self.sent_max).zero_()
        self.done_beams = [[[] for _ in range(self.sent_max)]  for _ in range(batch_size)]
        for k in range(batch_size):
            sen_h, sen_c = self.init_sent_hidden(beam_size)
            tmp_topic_feats = pool_feats[k:k + 1].expand(beam_size, pool_feats.size(1))
            tmp_att_feats = att_feats[k:k + 1].expand(*(beam_size,)+ att_feats.size()[1:])
            tmp_p_att_feats = p_att_feats[k:k + 1].expand(*(beam_size,)+ p_att_feats.size()[1:])
            tmp_guide_vector = guide_vector[k:k + 1].expand(beam_size, guide_vector.size(1))
            # ************add copy***********
            tmp_copy_hidden = copy_hidden[k*self.opt.copy_num:(k+1)*self.opt.copy_num].unsqueeze(0).expand(
                              *((beam_size,self.opt.copy_num,)+copy_hidden.size()[1:])).contiguous().view(
                                *((beam_size* self.opt.copy_num,) + copy_hidden.size()[1:]))
            tmp_copy_mask = copy_mask[k*self.opt.copy_num:(k+1)*self.opt.copy_num].unsqueeze(0).expand(
                            *((beam_size,self.opt.copy_num,)+copy_mask.size()[1:])).contiguous().view(
                            *((beam_size* self.opt.copy_num,) + copy_mask.size()[1:]))
            tmp_copy_freq = copy_freq[k:k+1].expand(beam_size,copy_freq.size(1)).contiguous()
            tmp_copy_label_tmp = copy_label_tmp[k*self.opt.copy_num:(k+1)*self.opt.copy_num].unsqueeze(0).expand(
                            *((beam_size,self.opt.copy_num,)+copy_label_tmp.size()[1:])).contiguous().view(
                            *((beam_size* self.opt.copy_num,) + copy_label_tmp.size()[1:]))
            tmp_copy_label = tmp_copy_label_tmp.view(beam_size, -1)
            tmp_dis_label = dis_label[k*self.opt.copy_num:(k+1)*self.opt.copy_num].unsqueeze(0).expand(
                            *((beam_size, self.opt.copy_num,) + dis_label.size()[1:])).contiguous().view(
                            *((beam_size * self.opt.copy_num,) + dis_label.size()[1:]))

            tmp_copy = [copy_flag, tmp_copy_label_tmp,tmp_copy_label,tmp_dis_label, tmp_copy_mask,
                                                           tmp_copy_freq, tmp_copy_hidden, dis_model]
            seq_masks = Variable(feats.data.new(beam_size, self.word_max - 1)
                                 .zero_(), requires_grad=False).cuda()
            att_weights = Variable(feats.data.new(beam_size,
                                                  self.word_max - 1, region_num).zero_())

            visual_key = tmp_p_att_feats
            visual_value = tmp_att_feats
            # lang_key = self.lang_embed(tmp_dense_hidden)
            # lang_value = tmp_dense_hidden
            kv_state = self.init_word_lstm2_hidden(beam_size)

            for s in range(self.sent_max):
                sen_h, sen_c = self.sen_lstm(tmp_topic_feats, (sen_h, sen_c))
                topic_vector = self.topic_layer(sen_h)
                logic_end = self.pre_prob(sen_h)  # .view(-1)
                logic_end = self.softmax(logic_end)
                init_word_lstm2_hidden = self.init_word_lstm2_hidden(beam_size)
                state = (init_word_lstm2_hidden, init_word_lstm2_hidden)
                if s == 0:
                    sent_end[k, s] = 1
                elif s >= 1:
                    if s == 1:
                        sent_unfinished = logic_end[:, 1] > stop_value
                    else:
                        sent_unfinished = sent_unfinished * (logic_end[:, 1] > stop_value)
                    sent_end[k, s] = sent_unfinished[-1]
                    if sent_unfinished.sum() == 0:
                        break

                # input <BOS>
                it = feats.data.new(beam_size).long().fill_(1)
                xt = self.embed(it)
                output, state, visual_context, kv_state = self.word_lstm(xt, topic_vector,tmp_guide_vector, visual_value, visual_key,state, self.kv_attention, kv_state)
                visual_key, _ = self.updata_kv_step(output, visual_key, visual_value)

                h_lang = state[0][-1]  # [batch,512] get the language LSTM hidden state
                h_lang = h_lang.unsqueeze(1).expand(
                    *((h_lang.size(0), self.opt.copy_num) + h_lang.size()[1:])).contiguous().view(
                    *((h_lang.size(0) * self.opt.copy_num,) + h_lang.size()[1:])).unsqueeze(1)  # # [batch*5,1,512]


                gen_factor = self.generating_factor(torch.cat([visual_context, output], 1))
                gen_factor = self.sigmoid(gen_factor)

                copy_prob = self.copy_att(h_lang, copy_hidden, copy_mask)  # (batch*5, 1, max(copy_len))
                copy_prob = copy_prob.view(batch_size, -1)  # (batch, max(copy_len)*5)

                gen_prob = self.softmax(self.logit(output))
                if copy_flag:
                    gen_prob = gen_prob * gen_factor
                    copy_prob = self.softmax(copy_prob * (1 - gen_factor))
                    merge_prob = gen_prob.scatter_add_(1, copy_label, copy_prob)
                else:
                    merge_prob = gen_prob

                logprobs = torch.log(merge_prob, dim=1)

                if s>=1:
                   generated_seq = seq[k,:s,:]
                else:
                    generated_seq = feats.data.new(1, self.word_max-1).zero_()
                self.done_beams[k][s],visual_key,kv_state = self.beam_search(state, logprobs, generated_seq,
                            tmp_copy,topic_vector,tmp_guide_vector,visual_value, visual_key, kv_state, opt=opt)
                seq[k, s, :] = self.done_beams[k][s][0]['seq']
                seqLogprobs[k, s, :] = self.done_beams[k][s][0]['logps']

                # update the value memory
                for m,n in enumerate(self.done_beams[k][s]):
                    seq_len = sum(n['seq']!=0)
                    seq_masks[m,:seq_len] = 1


        last_sent_end = sent_end.unsqueeze(2)
        last_seq = seq * last_sent_end
        return last_seq,seqLogprobs, None



    def sample(self,feats,var_copy, copy_flag,dis_model=None,dis_label=None, opt={}):
        sample_max = opt.get('sample_max',1)
        beam_size = opt.get('beam_size',1)
        temperature = opt.get('tempeture',1.0)
        batch_size = feats.size(0)
        region_num = feats.size(1)
        feat_dim = feats.size(2)
        stop_value = opt.get('stop_value',0.5) # sentence rnn stop when predict_end<0.5
        if beam_size > 1:
            return self.sample_beam(feats,var_copy, copy_flag,dis_model,dis_label, opt)
        sen_h, sen_c = self.init_sent_hidden(batch_size)

        copy_hidden = self.copynet_encode(var_copy[0], var_copy[2],
                                          var_copy[3])  # [batch*5,15,1024]
        # truncate copy_labels and copy_masks and to the same size
        copy_label = var_copy[0][:, :copy_hidden.size(1)]  # [batch*5,max(copy_len)]
        copy_mask = var_copy[1][:, :copy_hidden.size(1)]  # [batch*5,max(copy_len)]
        copy_freq = var_copy[4][:, :copy_hidden.size(1)]  # [batch*5,max(copy_len)]
        copy_freq = copy_freq.contiguous().view(batch_size,-1)  # (batch,  max(copy_len)*5)

        copy_label_tmp = torch.where((copy_label - 2) > 0, copy_label - 2, torch.
                                 full_like(copy_label, 0))  # [batch*5,max(copy_len)]
        copy_label = copy_label_tmp.view(batch_size, -1)  # (batch, 1, max(copy_len)*5)
        pool_feats = self.feat_topic_embed(feats).max(dim=1)[0]  # [batch,1024]
        predict_seq = feats.data.new(batch_size, self.sent_max, self.word_max-1).long().zero_()  # [batch, sent_max, word_max]
        seqLogprobs = feats.data.new(batch_size, self.sent_max, self.word_max-1).zero_()
        att_weights = Variable(feats.data.new(batch_size,
                                              self.word_max - 1, region_num).zero_())  # [batch, word_max-1,50]
        seq_masks = Variable(feats.data.new(batch_size, self.sent_max,self.word_max - 1)
                                              .zero_(), requires_grad=False).cuda()  # [batch, sent_max, word_max-1]
        sent_unfinished = 1
        predict_topic = feats.data.new(batch_size, self.sent_max, self.word_rnn_size).zero_()  # [batch, sent_max, word_rnn_size]

        # visual feats embed
        feats = feats.view(-1, feat_dim)  # [batch*50,4096]
        att_feats = self.att_embed(feats).view(batch_size, -1, self.word_rnn_size)  # [batch*50,512]-->[batch,50,512]
        p_att_feats = self.ctx2att(att_feats)  # [batch,50,att_hid_size]

        visual_key = p_att_feats
        visual_value = att_feats
        visual_kv_state = self.init_word_lstm2_hidden(batch_size)

        for i in range(self.sent_max):
            sen_h, sen_c = self.sen_lstm(pool_feats,(sen_h, sen_c))
            topic_vector = self.topic_layer(sen_h)
            predict_topic[:, i,:] = topic_vector
            logic_end = self.pre_prob(sen_h)
            logic_end = self.softmax(logic_end)
            init_word_lstm2_hidden = self.init_word_lstm2_hidden(batch_size)
            state = (init_word_lstm2_hidden, init_word_lstm2_hidden)
            if i >= 1:
                if i == 1:
                    sent_unfinished = logic_end[:, 1] > stop_value
                else:
                    sent_unfinished = sent_unfinished * (logic_end[:, 1] > stop_value)
                if sent_unfinished.sum() == 0:
                    break

            for j in range(self.word_max):

                if j == 0:
                    it = feats.data.new(batch_size).long().fill_(1)
                elif sample_max:
                    sampleLogprobs, it = torch.max(logprobs.data, 1)
                    # NOTE: the output of logit start from <EOS> 2, not same with embedding start from <PAD> 0
                    it = it.view(-1).long()
                else:
                    if temperature == 1.0:
                        prob_prev = torch.exp(
                            logprobs.data)  # fetch prev distribution: shape [batch*5,vocab_size+1]
                    else:
                        # scale logprobs by temperature
                        prob_prev = torch.exp(torch.div(logprobs.data, temperature))
                    it = torch.multinomial(prob_prev, 1)  # [batch,1]
                    sampleLogprobs = logprobs.gather(1, it)  # [batch,1]  gather the logprobs at sampled positions
                    it = it.view(-1).long()

                xt = self.embed(it)

                if j >= 1:
                    if j == 1:
                        word_unifished = (it != 2) * sent_unfinished
                    else:
                        word_unifished = word_unifished * (it != 2)   # [batch]
                    seq_masks[:,i,j-1] = word_unifished
                    if word_unifished.sum() == 0:
                        break
                    it = it * word_unifished.type_as(it)
                    predict_seq[:, i, j - 1] = it
                    seqLogprobs[:, i, j - 1] = sampleLogprobs.view(-1)
                for round_d in range(self.round):
                    output, state, visual_context, visual_kv_state = self.word_lstm(xt, topic_vector, visual_value, visual_key, state,
                                                                    self.kv_attention, visual_kv_state,round_d)
                    visual_key, _ = self.updata_kv_step(output, visual_key, visual_value)

                h_lang = state[0][-1]  # [batch,512] get the language LSTM hidden state
                h_lang = h_lang.unsqueeze(1).expand(
                    *((h_lang.size(0), self.opt.copy_num) + h_lang.size()[1:])).contiguous().view(
                    *((h_lang.size(0) * self.opt.copy_num,) + h_lang.size()[1:])).unsqueeze(1)  # # [batch*5,1,512]

                gen_factor = self.generating_factor(torch.cat([visual_context, output], 1))
                gen_factor = self.sigmoid(gen_factor)

                copy_prob = self.copy_att(h_lang, copy_hidden, copy_mask)  # (batch*5, 1, max(copy_len))
                copy_prob = copy_prob.view(batch_size, -1)  # (batch, max(copy_len)*5)

                gen_prob = self.softmax(self.logit(output))
                if copy_flag:
                    gen_prob = gen_prob * gen_factor
                    copy_prob = self.softmax(copy_prob * (1 - gen_factor))
                    merge_prob = gen_prob.scatter_add_(1, copy_label, copy_prob)
                else:
                    merge_prob = gen_prob

                logprobs = torch.log(merge_prob, dim=1)


        return predict_seq,seqLogprobs,predict_topic  # word from <PAD> 0
