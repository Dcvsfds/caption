from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
from torch.autograd import Variable
import torch.optim as optim
import numpy as np
import logging
import time
import os
from six.moves import cPickle
from tensorboardX import SummaryWriter

import opts
import models
import torch.nn as nn
import eval_utils
import misc.utils as utils
import torch.nn.functional as F
from tensorboardX import SummaryWriter
# from torch.nn.parallel.data_parallel import DataParallel
from misc.rewards import get_self_critical_reward
from .discriminator import Discriminator,generate_batch_sample

def adv_train(opt,model,dis_model):
    logger = utils.set_log('scst')
    from dataloader_gen import DataLoader_Gen  # # load pre-processed features
    loader_gen = DataLoader_Gen(opt)
    from dataloader_dis import DataLoader_Dis  # # load pre-processed features
    loader_dis = DataLoader_Dis(opt)

    g_step = opt.g_steps
    d_step = opt.d_steps
    gen_optimizer = optim.Adam(model.parameters(), lr=opt.scst_lr_generator)
    dis_optimizer = optim.Adam(dis_model.parameters(), lr=opt.scst_lr_dis)
    if os.path.isfile(os.path.join('save_xe/optimizer.pth')):
        print('load pretrain generator optimizer.pth')
        gen_optimizer.load_state_dict(torch.load(os.path.join('save_xe/optimizer.pth')))
    rl_crit = utils.RewardCriterion()
    crit = utils.Criterion(opt)

    update_lr_flag = True
    alpha = opt.score_alpha
    best_val_score = None

    infos = {}
    if opt.start_from is not None:
        # open old infos and check if models are compatible
        print('load infos')
        with open(os.path.join(opt.start_from, 'infos-gan.pkl'), 'rb') as f:
            infos = cPickle.load(f)
        print('load models')
        model.load_state_dict(torch.load(os.path.join(opt.start_from, 'model-gan.pth')))
        print('load dis_models')
        dis_model.load_state_dict(torch.load(os.path.join(opt.start_from, 'dis-model-gan.pth')))
        print('load optimizer.pth')
        gen_optimizer.load_state_dict(torch.load(os.path.join(opt.start_from, 'optimizer-gan.pth')))
        dis_optimizer.load_state_dict(torch.load(os.path.join(opt.start_from, 'dis-optimizer-gan.pth')))

    iteration = infos.get('iter', 0)
    epoch = infos.get('epoch', 0)
    loader_gen.iterators = infos.get('iterators', loader_gen.iterators)
    loader_gen.split_ix = infos.get('split_ix', loader_gen.split_ix)
    if opt.load_best_score == 1:
        best_val_score = infos.get('best_val_score', None)


    while True:
       for i in range(g_step):
           # print('train generator')
           if update_lr_flag:
               if epoch > opt.learning_rate_decay_start and opt.learning_rate_decay_start >= 0:
                   frac = (epoch - opt.learning_rate_decay_start) // opt.learning_rate_decay_every
                   decay_factor = opt.learning_rate_decay_rate ** frac
                   opt.current_model_lr = opt.scst_lr_generator * decay_factor
                   utils.set_lr(gen_optimizer, opt.current_model_lr)  # set the decayed rate

                   opt.current_dis_lr = opt.scst_lr_dis * decay_factor
                   utils.set_lr(dis_optimizer, opt.current_dis_lr)  # set the decayed rate
               else:
                   opt.current_model_lr = opt.scst_lr_generator
                   opt.current_dis_lr = opt.scst_lr_dis
               if epoch > opt.scheduled_sampling_start and opt.scheduled_sampling_start >= 0:
                   frac = (epoch - opt.scheduled_sampling_start) // opt.scheduled_sampling_increase_every
                   opt.ss_prob = min(opt.scheduled_sampling_increase_prob * frac, opt.scheduled_sampling_max_prob)
                   model.ss_prob = opt.ss_prob
               update_lr_flag = False

           model.train()
           dis_model.eval()

           start = time.time()
           torch.cuda.synchronize()
           copy_flag = opt.copy_adv
           gen_optimizer.zero_grad()
           data = loader_gen.get_batch('train')
           feats, labels, masks, sent_place, var_copy, dis_label = utils.input_data(data)
           gen_result, sample_logprobs, predict_topic = model.sample(feats,
                                var_copy, copy_flag,dis_model=dis_model,dis_label=dis_label, opt={'sample_max': 0})
           reward,mean_adv,mean_sc = get_self_critical_reward(alpha,model,dis_model,dis_label,
                                       feats, predict_topic, var_copy, copy_flag, data, gen_result)
           loss = rl_crit(sample_logprobs, gen_result, Variable(torch.from_numpy(reward).float().cuda()))
           loss.backward()
           torch.nn.utils.clip_grad_norm(model.parameters(), opt.grad_clip)

           gen_optimizer.step()
           train_loss = loss.item()
           torch.cuda.synchronize()
           end = time.time()
           if iteration % 50 == 0:  # 1000
               print(
                   "iter {} (epoch {}), avg_reward = {:.3f},adv_reward = {:.3f},sc_reward = {:.3f}, train_loss = {:.3f}, time/batch = {:.3f}" \
                   .format(iteration, epoch, np.mean(reward[:, 0]),mean_adv,mean_sc, train_loss, end - start))
               logger.info("gan training iter {} (epoch {}),avg_reward = {:.3f},adv_reward = {:.3f},sc_reward = {:.3f}, train_loss = {:.3f}" \
                            .format(iteration, epoch,np.mean(reward[:, 0]),mean_adv,mean_sc, train_loss))
               # writer.add_scalar('Train/train_gan_loss', train_loss, iteration)

           iteration += 1
           if (iteration % opt.save_checkpoint_every == 0):
               eval_kwargs = {'split': 'val', 'dataset': opt.input_json, 'verbose': True}
               eval_kwargs.update(vars(opt))
               # eval_kwargs['beam_size']=2
               _, _, _, predictions, lang_stats = eval_utils.eval_split(model, crit,
                                                                        loader_gen, copy_flag, eval_kwargs)
               logger.info(
                   "Bleu_1 is {0:.3f}, Bleu_2 is {1:.3f},Bleu_3 is {2:.3f}, Bleu_4 is {3:.3f},METEOR is {4:.3f}, ROUGE_L is {5:.3f}, CIDEr is {6:.3f} " \
                       .format(lang_stats['Bleu_1'], lang_stats['Bleu_2'], lang_stats['Bleu_3'], lang_stats['Bleu_4'],
                               lang_stats['METEOR'], lang_stats['ROUGE_L'], lang_stats['CIDEr']))

               if opt.language_eval == 1:
                   current_score = lang_stats['CIDEr']

               best_flag = False
               if True:
                   if best_val_score is None or current_score > best_val_score:
                       best_val_score = current_score
                       best_flag = True
                   if not os.path.exists(opt.checkpoint_path_scst):
                       os.makedirs(opt.checkpoint_path_scst)
                   checkpoint_path = os.path.join(opt.checkpoint_path_scst, 'model-gan.pth')
                   torch.save(model.state_dict(), checkpoint_path)
                   print("model saved to {}".format(checkpoint_path))

                   optimizer_path = os.path.join(opt.checkpoint_path_scst, 'optimizer-gan.pth')
                   torch.save(gen_optimizer.state_dict(), optimizer_path)

                   dis_checkpoint_path = os.path.join(opt.checkpoint_path_scst, 'dis-model-gan.pth')
                   torch.save(dis_model.state_dict(), dis_checkpoint_path)
                   print("dis model saved to {}".format(dis_checkpoint_path))
                   dis_optimizer_path = os.path.join(opt.checkpoint_path_scst, 'dis-optimizer-gan.pth')
                   torch.save(dis_optimizer.state_dict(), dis_optimizer_path)

                   infos['iter'] = iteration
                   infos['epoch'] = epoch
                   infos['iterators'] = loader_gen.iterators
                   infos['split_ix'] = loader_gen.split_ix
                   infos['best_val_score'] = best_val_score
                   infos['opt'] = opt
                   infos['vocab'] = loader_gen.get_vocab()
                   with open(os.path.join(opt.checkpoint_path_scst, 'infos-gan.pkl'), 'wb') as f:
                       cPickle.dump(infos, f)

                   if best_flag:
                       checkpoint_path = os.path.join(opt.checkpoint_path_scst, 'model-gan-best.pth')
                       torch.save(model.state_dict(), checkpoint_path)
                       print("model saved to {}".format(checkpoint_path))
                       optimizer_path = os.path.join(opt.checkpoint_path_scst, 'optimizer-gan-best.pth')
                       torch.save(gen_optimizer.state_dict(), optimizer_path)
                       dis_checkpoint_path = os.path.join(opt.checkpoint_path_scst, 'dis-model-gan-best.pth')
                       torch.save(dis_model.state_dict(), dis_checkpoint_path)
                       dis_optimizer_path = os.path.join(opt.checkpoint_path_scst, 'dis-optimizer-gan-best.pth')
                       torch.save(dis_optimizer.state_dict(), dis_optimizer_path)

                       print("dis model saved to {}".format(dis_checkpoint_path))
                       with open(os.path.join(opt.checkpoint_path_scst, 'infos-gan-best.pkl'), 'wb') as f:
                           cPickle.dump(infos, f)

           if data['bounds']['wrapped']:
               epoch += 1
               update_lr_flag = True
           if epoch >= opt.adv_trian_epoch :
               break

       for i in range(d_step):
           print('train discriminator')
           data = loader_dis.get_sample_batch('train',random_sample=True)
           copy_flag = opt.copy_adv
           dis_model.train()
           dis_optimizer.zero_grad()
           seq, topic_feats, gt, dis_label = generate_batch_sample(model, data, copy_flag, opt)
           dis_loss, _,_ = dis_model(seq, topic_feats, gt, dis_label, mode='train_dis')
           if iteration % 100 == 0:
               logger.info("gan discriminator training iter {} (epoch {}), train_loss = {:.3f}" \
                            .format(iteration, epoch, dis_loss.item()))
               print("gan discriminator training iter {} (epoch {}), train_loss = {:.3f}" \
                            .format(iteration, epoch, dis_loss.item()))
           dis_loss.backward()
           utils.clip_gradient(dis_optimizer, opt.grad_clip_dis)
           dis_optimizer.step()