import torch
import torchvision.models as models
import torch.nn as nn
import torch.utils.data
from sklearn.neighbors import NearestNeighbors
from numpy import argmax
import pickle,json
import numpy as np
import os
import time
from json import encoder
from tqdm import tqdm
from collections import OrderedDict
from cider.pyciderevalcap.ciderD.ciderD import CiderD
CiderD_scorer = None
import sys
from PIL import Image
import matplotlib.pyplot as plt
from multiprocessing import Pool
import multiprocessing
import h5py

def chunks(l, n):
    """
    Return a generator of lists, each of size n (the last list may be less than n)
    """
    for i in range(0, len(l), n):
        yield l[i:i + n]



def init_scorer(cached_tokens):
    global CiderD_scorer
    CiderD_scorer = CiderD_scorer or CiderD(df=cached_tokens)
def array_to_str(arr):
    out = ''
    for i in range(len(arr)):
        out += str(arr[i]) + ' '
        if arr[i] == 0:
            break
    return out.strip()
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def save_features(data_path,input_json):
        imgs = json.load(open(input_json, 'r'))
        att_feats = h5py.File(data_path, 'r')
        imgs = imgs['images']

        img_test = []
        img_train = []
        img_val = []
        for cnt,img in enumerate(imgs):
            print('\rnow propocess images :{}/{}'.format(cnt, len(imgs)), end='')
            img_id = img['id']
            img_feats = np.array(att_feats['feats'][str(img_id)])
            img_fc = img_feats.mean(axis=0)
            # img_fc = img_feats.max(axis=0)
            if img['split'] == 'test':
                img_test.append(img_fc)
            elif img['split'] == 'val':
                img_val.append(img_fc)
            elif img['split'] in ['train']:
                img_train.append(img_fc)

        pickle.dump((img_train), open(os.path.join('data','exact_features','train'+'_feature.pkl'), 'wb'))
        pickle.dump((img_val), open(os.path.join('data','exact_features','val'+'_feature.pkl'), 'wb'))
        pickle.dump((img_test), open(os.path.join('data','exact_features','test'+'_feature.pkl'), 'wb'))



def neighbor(data_file,split,k,precess_num=1000):

    neigh = NearestNeighbors(n_neighbors=k, metric='cosine')

    indxes =[]
    train_features = pickle.load(open(os.path.join(data_file,'exact_features','train_feature.pkl'), 'rb'))
    if split == 'train':
        img_features = train_features
    else:
        img_features = pickle.load(open(os.path.join(data_file,'exact_features',split+'_feature.pkl'), 'rb'))

    neigh.fit(train_features)
    samples = chunks(img_features,precess_num)
    for sample in samples:
        diatance,query_img_indices = neigh.kneighbors(sample,
                                             return_distance=True)
        if split == 'train':
            indx = query_img_indices[:,1:]
            indxes.extend(indx)   # shape :(len(img),30)
        else:
            indx = query_img_indices[:, :k-1]
            indxes.extend(indx)  # shape :(len(img),30)
    return indxes





def processs(indxes,train_caption,image_filename,current_start,top_k):
    result = []
    for cnt, indx in enumerate(indxes):
        print('\rnow propocess images :{}/{}'.format(cnt, len(indxes)), end='')
        candidate_captionings = []
        all_scores = []

        for i in indx:
            candidate_captionings.extend([caption for caption in
                                          train_caption[i][0]])

        hyp_str_captions = OrderedDict()
        ref_str_captions = OrderedDict()
        for m in range(len(candidate_captionings)):
            hyp_str_captions[m] = [candidate_captionings[m]]
        ptr = 0  # point to the index of the candidate_captionings
        for i in range(len(indx)):

            mask = np.zeros(len(candidate_captionings), dtype=bool)
            caption_per_process = len(train_caption[indx[i]][0])
            mask[ptr:(ptr+ caption_per_process)] = True

            hyp_captions = np.array(candidate_captionings)[
                mask].tolist()
            ref_captions = np.array(candidate_captionings)[np.invert(mask)].tolist()


            length = len(hyp_captions)  # 5
            for m in range(length):
                ref_str_captions[m + ptr] = [ref_captions[j] for j in
                                                    range(len(ref_captions))]  # shape:{0:[],1:[],...,149:[]}
            ptr = ptr + caption_per_process
            # ref_str_captions = {j: [array_to_str(ref_captions[i]) for i in range(len(ref_captions))] for j in
            #                     range(len(hyp_captions))}
        hyp_str_captions = [{'image_id': i, 'caption': hyp_str_captions[i]} for i in range(len(candidate_captionings))]
        _, cider_scores = CiderD_scorer.compute_score(ref_str_captions, hyp_str_captions)

        all_scores.extend(cider_scores)
        score_sort = np.argsort(-cider_scores, axis=0)
        topk_indx = score_sort[:top_k]
        out_caption = [candidate_captionings[i] for i in topk_indx]
        result.append({'caption': out_caption, 'filename': image_filename[cnt+current_start]})
    return result





def multi_process(indxes,split,train_caption,image_filename,max_process,top_k,caption_per_process = 5):
    num_process = min(multiprocessing.cpu_count(),max_process)
    img_nums = len(indxes)
    interval = int(img_nums / num_process) + 1  # add 1 to avoid missing data
    process_label = np.arange(0,img_nums,interval)
    assert  len(process_label)==num_process,print('process_label not equal num_process')  # equal
    p = Pool()
    restlt_ = []
    result_last = []
    for i,lab  in enumerate(process_label):
        if i == len(process_label)-1:
            indx = indxes[process_label[i]:]
        else:
            indx = indxes[process_label[i]:process_label[i+1]]
        res = p.apply_async(processs, args=(indx,train_caption,image_filename,lab,top_k))
        restlt_.append(res)
    p.close()
    p.join()
    for i in restlt_:
        result_last.extend(i.get())
    return result_last






def get_similar_caption(data_file,cluster_num,top_k,sent_all):
    with open(os.path.join(data_file, 'image_train_captions+path+filename' + '.json'), 'r') as j:
        caption_path = json.load(j)

    train_caption = caption_path['captions']
    train_name = caption_path['img_filename']
    result = {'train':[],'val':[],'test':[]}
    init_scorer('para-train-words')
    for split in ['train', 'val', 'test']:
    # for split in [ 'test','val', 'train',]:
        with open(os.path.join(data_file,'image_'+split+'_captions+path+filename'+'.json'), 'r') as j:
            caption_path = json.load(j)

        image_filename = caption_path['img_filename']
        gt_caps = caption_path['captions']

        indxes = neighbor(data_file,split,cluster_num)
        out = multi_process(indxes, split, train_caption, image_filename, max_process, top_k)

        result[split].extend(out)
    return result

def generate_new_json(data_path,result):

    a = json.load(open(data_path,'r'))
    image_infos = a['images']
    iterator = {'train':0,'val':0,'test':0}
    out = []
    for img in tqdm(image_infos):
        tmp = {}
        if img['split'] in ['train', 'restval']:
            assert img['filename'] == result['train'][iterator['train']]['filename']
            id = img['filename'].split('.')[0]
            retrieval = result['train'][iterator['train']]['caption'][0]
            iterator['train'] = iterator['train']+1
        elif img['split'] in ['val']:
            assert img['filename'] == result['val'][iterator['val']]['filename']
            id = img['filename'].split('.')[0]
            retrieval = result['val'][iterator['val']]['caption'][0]
            iterator['val'] = iterator['val']+1
        elif img['split'] in ['test']:
            assert img['filename'] == result['test'][iterator['test']]['filename']
            id = img['filename'].split('.')[0]
            retrieval = result['test'][iterator['test']]['caption'][0]
            iterator['test'] = iterator['test']+1
        tmp['id'] = id
        tmp['retrieval'] = retrieval
        out.append(tmp)
    with open( 'retrieval_paragraph.json', 'w') as w:
        json.dump({'images':out}, w)


def language_eval(dataset, preds, split):
    sys.path.append("coco_caption")
    annFile = 'coco_caption/annotations/para_captions_test.json'
    from coco_caption.pycocotools.coco import COCO
    from coco_caption.pycocoevalcap.eval import COCOEvalCap

    encoder.FLOAT_REPR = lambda o: format(o, '.3f')

    if not os.path.isdir('eval_results'):
        os.mkdir('eval_results')
    cache_path = os.path.join('eval_results/','clustering' + '_' + split + '.json')

    coco = COCO(annFile)
    valids = coco.getImgIds()

    # filter results to only those in MSCOCO validation set (will be about a third)
    preds_filt = [p for p in preds if p['image_id'] in valids]
    print('using %d/%d predictions' % (len(preds_filt), len(preds)))
    json.dump(preds_filt, open(cache_path, 'w'))  # serialize to temporary json file. Sigh, COCO API...

    cocoRes = coco.loadRes(cache_path)
    cocoEval = COCOEvalCap(coco, cocoRes)
    cocoEval.params['image_id'] = cocoRes.getImgIds()
    cocoEval.evaluate()

    # create output dictionary
    out = {}
    for metric, score in cocoEval.eval.items():
        out[metric] = score

    imgToEval = cocoEval.imgToEval
    for p in preds_filt:
        image_id, caption = p['image_id'], p['caption']
        imgToEval[image_id]['caption'] = caption
    with open(cache_path, 'w') as outfile:
        json.dump({'overall': out, 'imgToEval': imgToEval}, outfile)

    return out

def process_out(out):
    s = ''
    for i,j in enumerate(out):
        if i == 0:
            s = s + j
        else:
            s = s + ' ' + j
    return s




if __name__=='__main__':
    cluster_num = 51
    max_process = 5
    top_k = 5
    sent_all = 80
    paragraph_file = 'data/para_karpathy_format.json'
    data_path =  'VG_feature.h5'
    save_features(data_path,paragraph_file)

    feats_path = 'data'
    result = get_similar_caption(feats_path, cluster_num, top_k,sent_all)

    # with open('data/cluster_paragraph_v1.json', 'w') as j:
    #     json.dump(result, j)
    with open('data/retrieval_paragraph.json', 'w') as j:
         json.dump(result, j)
    # # generate_new_json(paragraph_file, result)
    # eval('data/cluster_paragraph_v1.json')
