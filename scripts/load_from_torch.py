import json
import torch
import torch.nn as nn
import os
dense_parameters = json.load(open('../data/dense_parameters.json'))
weight = dense_parameters['word_embedding']
paragraph_words = json.load(open('../data/paratalk.json'))["ix_to_word"]
vg_words = dense_parameters['model_opt']["idx_to_token"]

outfile = os.path.join('../data', 'dense_parameters.json')

def exact_word_vector(paragraph_words,words,weights,outfile):
    # Note that <bos>, <eos>, and <pad> not in vg_words, but their word vectors still in weight
    # <bos> and <eos> have the same word vectors
    # START_TOKEN = self.vocab_size + 1
    # END_TOKEN = self.vocab_size + 1
    # NULL_TOKEN = self.vocab_size + 2


    assert  len(words)==len(weights)-2, "len(words) and len(weight) should be equal"
    vector = {}
    weight_length = len(weights)
    vector['<PAD>'] = weights[weight_length-1]
    vector['<BOS>'] = weights[weight_length-2]
    vector['<EOS>'] = weights[weight_length - 2]
    vgw2i = {i:j for j,i in enumerate(words)}
    for i,word in enumerate(list(paragraph_words.values())[3:]):

        idx = vgw2i[word]
        vector[word] = weights[idx]
    dense_parameters['dense_word_vector'] = vector
    with open(outfile, 'w') as f:
        json.dump(dense_parameters, f)
exact_word_vector(paragraph_words,vg_words,weight,outfile)