from __future__ import print_function
import sys, os
import json
import spacy

# Import spacy tokenizer
spacy_en = spacy.load('en')

# Caption data directory
data = '../data/captions/'

# Files
para_json = os.path.join(data, 'paragraphs_v1.json')
train_split = os.path.join(data, 'train_split.json')
val_split = os.path.join(data, 'val_split.json')
test_split = os.path.join(data, 'test_split.json')

# Load data
para_json = json.load(open(para_json))
train_split = json.load(open(train_split))
val_split = json.load(open(val_split))
test_split = json.load(open(test_split))

# Helper function to get train/val/test split
def get_split(id):
    if id in train_split:
        return 'train'
    elif id in val_split:
        return 'val'
    elif id in test_split:
        return 'test'
    else:
        raise Exception('id not found in train/val/test')

# Tokenize the paragraphs and reformat them to a JSON file
# with the same format as the Karpathy MS COCO JSON file

def tokenize_and_reformat(para_json):
    images = []
    unique_ids = []
    cnt = 0
    for imgid, item in enumerate(para_json):

        sents = []
        if imgid % 1000 == 0:
            print('{}/{}'.format(imgid, len(para_json)))

        # Extract info
        filename = item['url'].split('/')[-1]  # filename also is: str(item['image_id']) + '.jpg'
        id       = item['image_id']            # visual genome image id (filename)

        # Skip duplicate paragraph captions
        if id in unique_ids:
            continue
        else:
            unique_ids.append(id)
        raw_paragraph = item['paragraph'].lower()
        each_paragraph = raw_paragraph
        sentences = each_paragraph.split('.')

        sentences = map(lambda sent: sent.strip(), sentences)
        sentences = list(filter(lambda sent: len(sent) >= 2, sentences))



        # Write info in coco_json format
        image = {}
        image['sentids']   = [id]               # only one ground truth paragraph
        image['filename']  = filename
        image['imgid']     = imgid
        image['split']     = get_split(id)
        image['id']        = id

        image['sentences'] = {}
        image['sentences']['tokens'] = sentences
        image['sentences']['raw']    = raw_paragraph
        images.append(image)

    paragraph_json = {
    'images': images,
    'dataset': 'para'
    }

    print('Finished tokenizing paragraphs.')
    print('There are {} duplicate captions.'.format(len(para_json) - len(unique_ids)))
    print('The dataset contains {} images and annotations'.format(len(paragraph_json['images'])))

    return paragraph_json

if __name__ == '__main__':
    print('This will take a couple of minutes.')
    paragraph_json = tokenize_and_reformat(para_json)
    outfile = os.path.join(data, 'para_karpathy_format.json')
    with open(outfile, 'w') as f:
        json.dump(paragraph_json, f)

